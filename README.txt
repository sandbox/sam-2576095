Amazon S3
=========

Example Configuration:

$settings['amazons3.access_key'] = '';
$settings['amazons3.secret'] = '';
$settings['amazons3.region'] = '';
$settings['amazons3.bucket'] = '';

To-do:

* Document usage of bucket in directory.
* Stub methods that don't work when path is NULL for s3://bucket/PATH.
