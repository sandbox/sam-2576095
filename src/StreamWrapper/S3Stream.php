<?php

/**
 * @file
 * Contains Drupal\amazons3\StreamWrapper\S3Stream.
 */

namespace Drupal\amazons3\StreamWrapper;

use Aws\S3\S3Client;
use Aws\S3\StreamWrapper;
use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Registers and amazon s3 stream wrpaper.
 */
class S3Stream extends StreamWrapper implements StreamWrapperInterface {

  use StringTranslationTrait;

  /**
   * The S3 client.
   *
   * @var \Aws\S3\S3Client
   */
  protected $client;

  /**
   * Construct an instance of the S3 stream.
   */
  public function __construct() {
    $this->client = new S3Client([
      'region' => Settings::get('amazons3.region', ''),
      'version' => '2006-03-01',
      'credentials' => [
        'key' => Settings::get('amazons3.access_key'),
        'secret' => Settings::get('amazons3.secret', ''),
      ],
    ]);
    $this->register($this->client);
  }

  /**
   * Instance URI (stream).
   *
   * @var string
   */
  protected $uri;

  /**
   * {@inheritdoc}
   */
  public function stream_lock($operation) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_metadata($path, $option, $value) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_set_option($option, $arg1, $arg2) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_truncate($new_size) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getType() {
    return StreamWrapperInterface::WRITE_VISIBLE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return t('S3');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('An amazon s3 stream wrapper.');
  }

  /**
   * {@inheritdoc}
   */
  public function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    $parsed = $this->parseUri($this->uri);
    $request = $this->client->createPresignedRequest($this->client->getCommand('GetObject', [
      'Bucket' => $parsed['bucket'],
      'Key'    => $parsed['key'],
    ]), '+1 week');
    return (string) $request->getUri();
  }

  /**
   * {@inheritdoc}
   */
  public function realpath() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function dirname($uri = NULL) {
    $parts = explode(DIRECTORY_SEPARATOR, $uri);
    array_pop($parts);
    return implode(DIRECTORY_SEPARATOR, $parts);
  }

  /**
   * @param $uri
   * @return array
   */
  protected function parseUri($uri) {
    $path = substr($uri, strlen('s3://'));
    $parts = explode(DIRECTORY_SEPARATOR, $path);
    return [
      'bucket' => array_shift($parts),
      'key' => implode($parts, DIRECTORY_SEPARATOR),
    ];
  }

}
